import os
from typing import Any, List, Mapping, Optional
from langchain_google_vertexai import ChatVertexAI
from langchain.utilities import SerpAPIWrapper
from langchain.agents import Tool
from langchain.memory import ConversationBufferMemory
from langchain.agents import AgentType
from langchain.agents import initialize_agent
from langchain_community.tools.tavily_search import TavilySearchResults
import vertexai
from dotenv import load_dotenv

load_dotenv()
vertexai.init(project=os.getenv("project"),location=os.getenv("location"))

def LLM_init(inpPrompt):

	llm = ChatVertexAI(model_name="chat-bison@002", temperature=0.6, max_output_tokens=1024, top_p=0.95, top_k=40)

	#search = SerpAPIWrapper()
	tools =[
        Tool(
            name="current search",
            func=TavilySearchResults(max_results=3),
            description="useful for when you need to answer questions about current events or the current state of the world"
        ),]

	memory = ConversationBufferMemory(memory_key="chat_history")

	llm_chain = initialize_agent(
        tools,
        agent=AgentType.CONVERSATIONAL_REACT_DESCRIPTION,
        llm=llm, 
        memory=memory, 
        verbose=True,)

	return llm_chain.invoke({"input": {inpPrompt}})

res = LLM_init("tell me top 5 latest world news")
print (res)