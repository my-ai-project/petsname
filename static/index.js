$(document).ready(function(){
    
    var mediaRecorder 
    //$('#recButton').addClass("notRec");

    $('#recButton').click(function(){
        if($('#recButton').hasClass('notRec')){
            navigator.mediaDevices
                .getUserMedia({audio: true})
                .then(stream => { mediaRecorder = new MediaRecorder(stream); 
            mediaRecorder.start();
            audioChunks = [];
            mediaRecorder.ondataavailable = (e) => {
                audioChunks.push(e.data);
            };

        });
            $('#recButton').removeClass("notRec");
            $('#recButton').addClass("Rec");
        }
        else{
            mediaRecorder.stop();
            mediaRecorder.onstop = (e) => {
                mediaRecorder.stream.getTracks().forEach( track => track.stop() );
                let blob = new Blob(audioChunks, {type: 'audio/mpeg-3'});
                sendVoice(blob);
            };
            $('#recButton').removeClass("Rec");
            $('#recButton').addClass("notRec");
        }
    }); 
});

function sendVoice(data) {
    var form = new FormData();
    form.append('mediaFile', data, 'data.mp3');
    //Chrome inspector shows that the post data includes a file and a title.
    $.ajax({
        type: 'POST',
        url: '/voice',
        data: form,
        cache: false,
        processData: false,
        contentType: false
    }).done(function(data) {
        console.log(data);
    });
}

function sendText(prompt) {
    var sendData = {convText: prompt};
    $('#chatBlock').append('<li class="type-loader left"><div><div class="typing"></div></div></li>');
    scrollChat();
    $.ajax({
            type: 'POST',
            url: '/text',
            dataType: 'json',
            contentType: "application/json",
            data:JSON.stringify(sendData),
            success: function(value) {
                $('#chatBlock').children('.type-loader').remove();
                $('#chatBlock').append('<li class="left"><div>'+value+'</div><i class="copier fa-solid fa-copy"></i></li>');
                $('#resummarizeBlock').css('display', 'flex');
                scrollChat();
            },

        });
}






















/*function handlerFunction(stream) {
    rec = new MediaRecorder(stream);
    rec.ondataavailable = e => {
        audioChunks.push(e.data);
        if (rec.state == "inactive") {
            let blob = new Blob(audioChunks, {type: 'audio/mpeg-3'});
            sendData(blob);
        }
    }
}

startRecording.onclick = e => {
    console.log('Recording are started..');
    startRecording.disabled = true;
    stopRecording.disabled = false;
    audioChunks = [];
    rec.start();
};

stopRecording.onclick = e => {
    console.log("Recording are stopped.");
    startRecording.disabled = false;
    stopRecording.disabled = true;
    rec.stop();
};
*/ 