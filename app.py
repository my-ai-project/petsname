from flask import Flask
from views import *
import os
from datetime import timedelta

app = Flask(__name__)
app.add_url_rule('/', view_func=home.as_view('home'))
app.add_url_rule('/voice', view_func=voice.as_view('voice'))
app.add_url_rule('/text', view_func=voice.as_view('text'))

if __name__ == '__main__':
    app.run()