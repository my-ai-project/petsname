import torch
from transformers import AutoModelForSpeechSeq2Seq, AutoProcessor, pipeline
from dotenv import load_dotenv
from llm import generate_text

load_dotenv()

device = "cuda:0" if torch.cuda.is_available() else "cpu"
torch_dtype = torch.float16 if torch.cuda.is_available() else torch.float32

model_id = "openai/whisper-large-v3"

model = AutoModelForSpeechSeq2Seq.from_pretrained(
    model_id, torch_dtype=torch_dtype, low_cpu_mem_usage=True, use_safetensors=True)
model.to(device)

processor = AutoProcessor.from_pretrained(model_id)

def autoSpeechRecon(path_to_file,chunk):
    pipe = pipeline(
    "automatic-speech-recognition",
    model=model,
    tokenizer=processor.tokenizer,
    feature_extractor=processor.feature_extractor,
    max_new_tokens=128,
    chunk_length_s=chunk,
    batch_size=16,
    return_timestamps=True,
    torch_dtype=torch_dtype,
    device=device)

    result = pipe(path_to_file)
    #print("you: "+result["text"])
    llm_res = generate_text(result["text"])
    return llm_res
    #return result['text']