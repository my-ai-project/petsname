from flask import render_template, request, jsonify
from flask.views import MethodView
#from playsound import playsound
import tempfile
from trans import autoSpeechRecon

class home(MethodView):
    def get(self):
        return render_template("index.html")

class voice(MethodView):
    def post(self):
        mediaFile = request.files['mediaFile']
        tempStorage = tempfile.gettempdir()+'/'+mediaFile.filename
        mediaFile.save(tempStorage) 
        if mediaFile:
            #playsound(tempStorage)
            transText = autoSpeechRecon(tempStorage,1024)
            return transText, 200
        else:
            return "media file not found", 400

class text(MethodView):
    def post(self):
        convText = request.json.get('convText')
        if convText:
            #playsound(tempStorage)
            replyText = generate_text(convText)
            return jsonify(replyText), 200
        else:
            return "request text not found", 400