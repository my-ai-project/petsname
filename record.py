import tkinter as tk
import pyaudio
import wave
import tempfile
import threading
from trans import autoSpeechRecon

chunk = 1024
sample_format = pyaudio.paInt16
channels = 2
fs = 44100


class voiceRecorder:
    def __init__(self):
        self.root = tk.Tk()
        self.root.title('Personal AI')
        self.root.geometry("200x200")
        self.btn1 = tk.Button(self.root, text="Record", command=self.clickHandler)
        self.btn1.pack(pady=20)
        self.lab1 = tk.Label(self.root, text="")
        self.lab1.pack()
        self.recording = False
        self.root.mainloop()

    def clickHandler(self):
        if self.recording:
            self.recording = False
            self.lab1.config(text = "Not Yet Listening!")
        else:
            self.recording = True
            threading.Thread(target=self.record).start()

    def record(self):
        self.lab1.config(text = "Listening...")
        p = pyaudio.PyAudio()
        frames = []

        stream = p.open(format=sample_format,
                    channels=channels,
                    rate=fs,
                    frames_per_buffer=chunk,
                    input=True)
        
        while self.recording:
            data = stream.read(chunk)
            frames.append(data)

        stream.stop_stream()
        stream.close()
        p.terminate()

        tempWavFile = tempfile.mktemp('.wav')
        wf = wave.open(tempWavFile, 'wb')
        wf.setnchannels(channels)
        wf.setsampwidth(p.get_sample_size(sample_format))
        wf.setframerate(fs)
        wf.writeframes(b''.join(frames))
        wf.close()
        autoSpeechRecon(tempWavFile,chunk)          


voiceRecorder()