import os
from dotenv import load_dotenv

from google.cloud import aiplatform
import vertexai
from vertexai.preview.generative_models import GenerativeModel, ChatSession, Part
from vertexai.language_models import ChatModel, InputOutputTextPair
from tts import player
from threading import Thread

load_dotenv()
vertexai.init(project=os.getenv("project"),location=os.getenv("location"))

def generate_text(newMsg) -> str:
    chat_model = ChatModel.from_pretrained("chat-bison@002")
    parameters = {
    "temperature": 0.6,  # Temperature controls the degree of randomness in token selection.
    "max_output_tokens": 1024,  # Token limit determines the maximum amount of text output.
    "top_p": 0.95,  # Tokens are selected from most probable to least until the sum of their probabilities equals the top_p value.
    "top_k": 40,  # A top_k of 1 means the selected token is the most probable among all tokens.
    }
    
    chat = chat_model.start_chat(
        context="Your name is GABREL, You are my personal assistant.",
        examples=[
            InputOutputTextPair(
                input_text="Who is your owner?",
                output_text="Mr.Hussain is my owner.",
            ),
            InputOutputTextPair(
                input_text="What is your purpose?",
                output_text="My purpose is to help and assist you.",
            ),
        ],

        )

    response = chat.send_message(newMsg,**parameters)
    #print("GIBREL :"+ response.text)
    thread = Thread(target = player, args = (response.text,))
    thread = thread.start()
    return response.text


# def generate_text() -> str:
#     #Loading Model
#     vision_model = GenerativeModel("gemini-pro")
#     # Generate text
#     response = vision_model.generate_content(
#         [
#             Part.from_uri(
#                 "gs://cloud-samples-data/video/animals.mp4", mime_type="video/mp4"
#             ),
#             "What is in the video?",
#         ]
#     )
#     print(response)
#     return response.text

#generate_text()