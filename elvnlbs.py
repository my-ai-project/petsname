from elevenlabs import Voice, VoiceSettings, generate, play, stream
#from llm import generate_text

def player(text="hi, I can help you"):
  #print(text)
  voice=Voice(
    voice_id='jsCqWAovK2LkecY7zXl4',
    settings=VoiceSettings(stability=0.65, similarity_boost=0.5, style=0.0, use_speaker_boost=True)
  )
  audio = generate(
    text=text,
    voice=voice,
    model="eleven_multilingual_v1",
  )

  play(audio)