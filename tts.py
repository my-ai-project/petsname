from dotenv import load_dotenv
from playsound import playsound
from transformers import SpeechT5Processor, SpeechT5ForTextToSpeech, SpeechT5HifiGan
from datasets import load_dataset
import torch
import tempfile
import soundfile as sf

load_dotenv()

processor = SpeechT5Processor.from_pretrained("microsoft/speecht5_tts")
model = SpeechT5ForTextToSpeech.from_pretrained("microsoft/speecht5_tts")
vocoder = SpeechT5HifiGan.from_pretrained("microsoft/speecht5_hifigan")

# load xvector containing speaker's voice characteristics from a dataset
embeddings_dataset = load_dataset("Matthijs/cmu-arctic-xvectors", split="validation")
speaker_embeddings = torch.tensor(embeddings_dataset[7306]["xvector"]).unsqueeze(0)

def player(genText="Hi, This is default text written here"):
	inputs = processor(text=genText, return_tensors="pt")
	speech = model.generate_speech(inputs["input_ids"], speaker_embeddings, vocoder=vocoder)
	tempWavFile = tempfile.mktemp('.wav')
	sf.write(tempWavFile, speech.numpy(), samplerate=16000)
	playsound(tempWavFile)
