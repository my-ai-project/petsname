import os
from dotenv import load_dotenv
from typing import Any, List, Mapping, Optional
import vertexai
from vertexai.preview.generative_models import GenerativeModel
from langchain.chains import ConversationChain
from langchain.memory import ConversationBufferMemory
from langchain.callbacks.manager import CallbackManagerForLLMRun
from langchain_core.language_models.llms import LLM
from langchain import hub
from langchain.agents import AgentExecutor, create_react_agent
from langchain.agents import Tool
from langchain.agents import AgentType
from langchain.agents import initialize_agent
from langchain.utilities import SerpAPIWrapper

load_dotenv()
vertexai.init(project=os.getenv("project"),location=os.getenv("location"))

class GeminiProLLM(LLM):
    @property
    def _llm_type(self) -> str:
        return "gemini-pro"

    def _call(
        self,
        prompt: str,
        stop: Optional[List[str]] = None,
        run_manager: Optional[CallbackManagerForLLMRun] = None,
        **kwargs: Any,
    ) -> str:
        # if stop is not None:
        #     raise ValueError("stop kwargs are not permitted.")
        
        gemini_pro_model = GenerativeModel("gemini-pro")

        model_response = gemini_pro_model.generate_content(
          prompt,
            generation_config={
          "temperature": 0.6,  # Temperature controls the degree of randomness in token selection.
          "max_output_tokens": 1024,  # Token limit determines the maximum amount of text output.
          "top_p": 0.95,  # Tokens are selected from most probable to least until the sum of their probabilities equals the top_p value.
          "top_k": 40,  # A top_k of 1 means the selected token is the most probable among all tokens.
          },
        )

        if len(model_response.candidates[0].content.parts) > 0:
            return model_response.candidates[0].content.parts[0].text
        else:
            return "<No answer given by Gemini Pro>"

    @property
    def _identifying_params(self) -> Mapping[str, Any]:
        """Get the identifying parameters."""
        return {"model_id": "gemini-pro", "temperature": 0.1}


def LLM_init():
    search = SerpAPIWrapper()
    tools =[
        Tool(
            name="current search",
            func=search.run,
            description="anything you do not know can be retrived."
        )]

    chain = initialize_agent(tools=tools,agent=AgentType.CONVERSATIONAL_REACT_DESCRIPTION,llm=GeminiProLLM(),
        memory=ConversationBufferMemory(memory_key="chat_history"), verbose=True)
    return chain
    

chatchain = LLM_init()
prompt = "Tell me the time?"

response = chatchain(prompt)
print(response)