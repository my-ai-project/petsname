import torch
from dotenv import load_dotenv
import tempfile
from TTS.api import TTS
from playsound import playsound

load_dotenv()
# Get device
device = "cuda" if torch.cuda.is_available() else "cpu"

# Init TTS
tts = TTS("tts_models/multilingual/multi-dataset/xtts_v2").to(device)


def player(genText="Hi, This is default text written here"):
	tempWavFile = tempfile.mktemp('.wav')
	tts.tts_to_file(text=genText, speaker_wav="/home/syedaashir/Downloads/friday.mp3", language="en", file_path=tempWavFile)
	playsound(tempWavFile)

player()